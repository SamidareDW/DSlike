﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour
{
   /* public float rotationSpeed;

    float angle = 0;
    float anglex = 0;

    void Start()
    {
    }
    

    void Update()
    {
        Rotation();

    }

    private void LateUpdate()
    {
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
    }

    void Rotation()
    {
        angle += Input.GetAxis("Vertical") * Time.deltaTime * rotationSpeed;
        anglex += Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed;

        angle = Mathf.Clamp(angle, -35, 40);
        //anglex = Mathf.Clamp(anglex, -35, 40);
        
        transform.localRotation = (Quaternion.AngleAxis(angle, Vector3.right)) * (Quaternion.AngleAxis(anglex, Vector3.up));

    }*/

   public float cameraDistanceMax = 20f;
   public float cameraDistanceMin = 5f;
   public float cameraDistance = 10f;
   public float scrollSpeed = 0.5f;
   
   public float angle;
   public Transform target;
   public float angularSpeed;
 
   [SerializeField]
   private Vector3 initialOffset;
 
   private Vector3 currentOffset;
 
   [ContextMenu("Set Current Offset")]
   private void SetCurrentOffset () {
       if(target == null) {
           return;
       }
 
       initialOffset = transform.position - target.position;
   }
 
   private void Start () {
       if(target == null) {
           Debug.LogError ("Assign a target for the camera in Unity's inspector");
       }
 
       currentOffset = initialOffset;
   }
 
   private void LateUpdate () {
       
       
       transform.position = target.position + currentOffset;
 
       float movement = Input.GetAxis ("Horizontal") * angularSpeed * Time.deltaTime;
       float movementup = Input.GetAxis ("Vertical") * angularSpeed * Time.deltaTime;
       if(!Mathf.Approximately (movement, 0f)) {
           transform.RotateAround (target.position, Vector3.up, movement);
           currentOffset = transform.position - target.position;
       }

       //ScrollZoom();

       /*if(Mathf.Approximately (movementup, 0f)) {
           transform.RotateAround (target.position, Vector3.right, movement);
           currentOffset = transform.position - target.position;
       }*/
   }

   void ScrollZoom()
   {
       cameraDistance += Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
       cameraDistance = Mathf.Clamp(cameraDistance, cameraDistanceMin, cameraDistanceMax);
   }
   
   void Rotation()
   {
       angle += Input.GetAxis("Vertical") * Time.deltaTime * angularSpeed;

       angle = Mathf.Clamp(angle, -15, 40);

       transform.localRotation = (Quaternion.AngleAxis(angle, Vector3.right));

   }
 
}
