﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISounds : MonoBehaviour
{
    
    AudioSource UIsounds;
    public AudioClip attackCommand;
    
    void Start()
    {
        UIsounds = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        
    }

    public void clickToAttack()
    {
        UIsounds.PlayOneShot(attackCommand, 0.8F);
    }
    
}
