﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform currentCharacter;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    

    void LateUpdate()
    {
        Vector3 desiredPosition = currentCharacter.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = currentCharacter.position + offset;
        transform.LookAt(currentCharacter);

    }

}
