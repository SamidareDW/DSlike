﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{

    public float dissapearAfter = 8;
    public float spellRange = 6;
    public int spellCost = 4;
    private AudioSource spellSound;
    [SerializeField]AudioClip fireballCast;
    [SerializeField]AudioClip fireballHit;
    
    void Start()
    {
        spellSound = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        DestroyAfterTime();
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            if (other.GetComponent<CharacterController>().currentHealth > 0)
                other.GetComponent<CharacterController>().CharacterTakeDamage();

            if (other.GetComponent<CharacterController>().currentHealth <= 0)
                other.GetComponent<CharacterController>().CharacterDeath();
            
            other.GetComponent<CharacterStats>().currentHealth -= 5;
            if (other.GetComponent<CharacterStats>().currentHealth <= 0)
                other.GetComponent<CharacterController>().Death();
            
            OnFireballHit();
            Destroy(gameObject);
        }
    }

    public void OnFireballCast()
    {
        spellSound = GetComponent<AudioSource>();
        spellSound.PlayOneShot(fireballCast, 0.7F);
    }

    void OnFireballHit()
    {
        spellSound = GetComponent<AudioSource>();
        spellSound.PlayOneShot(fireballHit, 1.0F);
    }

    void DestroyAfterTime()
    {
        dissapearAfter -= Time.deltaTime;
        if (dissapearAfter < 0)
            Destroy(gameObject);
    }
}
