﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

    public class StatsSystem : CombatLogic
    {
        [HideInInspector] public string name;
        public int maxHealth;
        public int currentHealth;
        public int maxMana;
        public int currentMana;

        public int expReward;

        private float timeLeft = 5;
        
        void Update()
        {
            if (currentHealth > 0)
            {
                DetectPlayer();
                Fighting();
            }


            if (currentHealth <= 0)
            {
                
                timeLeft -= Time.deltaTime;
                if (timeLeft < 0)
                    Destroy(gameObject);
                
            }
        }

        public void TakeDamage(int Damage)
        {
            //Debug.Log("Trafienie");
            if (CurrentEnemy.GetComponent<CharacterController>().currentHealth > 0)
                CurrentEnemy.GetComponent<CharacterController>().CharacterTakeDamage();

            if (CurrentEnemy.GetComponent<CharacterController>().currentHealth <= 0)
                CurrentEnemy.GetComponent<CharacterController>().CharacterDeath();
            
            CurrentEnemy.GetComponent<CharacterStats>().currentHealth -= 5;
            if (CurrentEnemy.GetComponent<CharacterStats>().currentHealth <= 0)
                CurrentEnemy.GetComponent<CharacterController>().Death();
        }

        public void Protection(int Protection, int Armor)
        {
            Protection = Armor;
        }

    }