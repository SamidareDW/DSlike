﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SkeletonStats: StatsSystem
{
    void Awake()
    {
        name = "Skeleton";

        expReward = 30;
        
        maxHealth = 13;
        currentHealth = maxHealth;
        maxMana = 0;
        currentMana = maxMana;
        
        detectionRange = 10;
        chasingRange = 20;
    }
}

public class Skeleton: SkeletonStats
{
    void Start()
    {

    }
    
    void Update()
    {
        if (!isDead)
        {
            DetectPlayer();
            Fighting();
        }
    }

}
