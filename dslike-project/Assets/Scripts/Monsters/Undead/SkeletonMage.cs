﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SkeletonMageStats: StatsSystem
{
    void Awake()
    {
        name = "Sorcerer";

        expReward = 25;
        
        maxHealth = 10;
        currentHealth = maxHealth;
        maxMana = 500000;
        currentMana = maxMana;
        
        detectionRange = 10;
        chasingRange = 20;
    }
}
public class SkeletonMage : SkeletonMageStats
{

    void Start()
    {
        
    }


    void Update()
    {
        if (!isDead)
        {
            DetectPlayer();
            SpellCasting();
        }
    }
}
