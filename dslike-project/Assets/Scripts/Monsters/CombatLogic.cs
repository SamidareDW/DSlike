﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class CombatLogic : MonsterSounds
{
    public Transform CurrentEnemy;
    [HideInInspector]public bool attackMode = false;
    [HideInInspector] public NavMeshAgent Agent;
    private int CurrentEnemyHealth;
    private int CurrentEnemyMaxHealth;
    public float detectionRange = 10;
    public float chasingRange = 20;
    public Rigidbody FireballSpell;
    [HideInInspector]public Animator Animator;
    public Transform castHand;
    [HideInInspector] public bool 
        isDead = false, isRunning = false, isAttacking = false;


    void Start()
    {
        
        
    }


    void Update()
    {
        
    }

    public void DetectPlayer()
    {
        Animator = GetComponent<Animator>();
        Agent = GetComponent<NavMeshAgent>();
        
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, detectionRange);
        if (!attackMode)
        {
            foreach (Collider hitCollider in hitColliders)
            {
                if (hitCollider.tag == "Player")
                {
                    //Debug.Log("W zasięgu");
                    CurrentEnemy = hitCollider.GetComponent<Transform>();
                    attackMode = true;
                }

            }
        }
    }

    public void Fighting()
    {
        Animator = GetComponent<Animator>();
        Agent = GetComponent<NavMeshAgent>();
        
        if (attackMode)
        {
            transform.LookAt(CurrentEnemy.transform);

            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) > 1.5) &&
                (Vector3.Distance(transform.position, CurrentEnemy.transform.position) < chasingRange))
            {
                isRunning = true;
                isAttacking = false;
                Agent.isStopped = false;
                Agent.destination = CurrentEnemy.position;
            }
            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) <= 1.5))
            {
                isRunning = false;
                isAttacking = true;
            }

            if (CurrentEnemy.GetComponent<CharacterStats>().currentHealth <= 0)
            {
                isAttacking = false;
                attackMode = false;
            }

            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) >= chasingRange))
            {
                CurrentEnemy = null;
                isRunning = false;
                isAttacking = false;
                attackMode = false;
                Agent.isStopped = true;
            }
        }

        Animator.SetBool("isRunning", isRunning);
        Animator.SetBool("isAttacking", isAttacking);
    }
    
    public void SpellCasting()
    {
        Animator = GetComponent<Animator>();
        Agent = GetComponent<NavMeshAgent>();
        
        if (attackMode)
        {
            transform.LookAt(CurrentEnemy.transform);

            Vector3 enemySpellDistance;
            enemySpellDistance = new Vector3(CurrentEnemy.position.x - spellRange, CurrentEnemy.position.y - spellRange, CurrentEnemy.position.z - spellRange);

            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) > spellRange) &&
                (Vector3.Distance(transform.position, CurrentEnemy.transform.position) < chasingRange))
            {
                isRunning = true;
                isAttacking = false;
                Agent.isStopped = false;
                Agent.destination = enemySpellDistance;
            }
            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) <= spellRange))
            {
                isRunning = false;
                isAttacking = true;
            }

            if (CurrentEnemy.GetComponent<CharacterStats>().currentHealth <= 0)
            {
                isAttacking = false;
                attackMode = false;
            }

            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) >= chasingRange))
            {
                CurrentEnemy = null;
                isRunning = false;
                isAttacking = false;
                attackMode = false;
                Agent.isStopped = true;
            }
        }

        Animator.SetBool("isRunning", isRunning);
        Animator.SetBool("isAttacking", isAttacking);
    }

    public void CastSpell()
    {
        GetComponent<StatsSystem>().currentMana -= spellCost;
        Rigidbody clone;
        clone = Instantiate(FireballSpell, castHand.position, transform.rotation);
        clone.transform.LookAt(CurrentEnemy);
        clone.velocity = transform.TransformDirection(Vector3.forward * 10);
        OnFireballCast();
    }
    
    public void Death()
    {
        Animator = GetComponent<Animator>();

        CurrentEnemy = null;
        isRunning = false;
        isAttacking = false;
        attackMode = false;
        Agent.isStopped = true;
        isDead = true;
        
        MonsterDeath();

        Animator.SetBool("isDead", isDead);
        GetComponent<NavMeshAgent>().enabled = !GetComponent<NavMeshAgent>().enabled;
    }
}


