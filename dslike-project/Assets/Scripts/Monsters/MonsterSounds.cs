﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSounds : Fireball
{
    public AudioSource Sounds;
    public AudioClip MonsterAttackSnd;
    public AudioClip MonsterTakeDamageSnd;
    public AudioClip MonsterTakeDamage2Snd;
    public AudioClip MonsterDamagedBySwordSnd;
    public AudioClip MonsterDamagedByBowSnd;
    public AudioClip MonsterDamagedByClubSnd;
    public AudioClip MonsterDeathSnd;

    public void MonsterAttack()
    {
        GetComponent<AudioSource>().PlayOneShot(MonsterAttackSnd, 0.8F);
    }
    
    public void MonsterTakeDamage()
    {
        GetComponent<AudioSource>().PlayOneShot(MonsterTakeDamageSnd, 1.0F);
    }
    public void MonsterDamagedBySword()
    {
        GetComponent<AudioSource>().PlayOneShot(MonsterDamagedBySwordSnd, 1.0F);
    }
    public void MonsterDamagedByBow()
    {
        GetComponent<AudioSource>().PlayOneShot(MonsterDamagedByBowSnd, 1.0F);
    }
    public void MonsterDamagedByClub()
    {
        GetComponent<AudioSource>().PlayOneShot(MonsterDamagedByClubSnd, 1.0F);
    }
    
    public void MonsterDeath()
    {
        GetComponent<AudioSource>().PlayOneShot(MonsterDeathSnd, 0.5F);
    }

}
