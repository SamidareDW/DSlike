﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


public class CharacterStats: CharacterStatsSystem
{
    
    void Awake()
    {
        LevelUp();
        level = 0;
        melee = 0;
        ranged = 0;
        magic = 0;
        
        maxHealth = 50;
        currentHealth = maxHealth;
        maxMana = 30;
        currentMana = maxMana;
    }
}


public class CharacterController : CharacterStats
{

    //public bool isDead = false;
    //public AudioClip deathSnd;
    //private AudioSource characterSounds;
    //private Animator Animator;
    //private NavMeshAgent Agent;
    //public Transform CurrentEnemy;
    //private Vector3 CurrentEnemyPos;
    //private string CurrentEnemyName;
    //private int CurrentEnemyHealth;
    //private int CurrentEnemyMaxHealth;
    //private UISounds InterfaceSounds;
    //private Collider characterCollider;

    //private bool isRunning = false;
    //private bool isAttacking = false;
    //private bool attackMode = false;

    void Start()
    {
        
    }


    void Update()
    {

        if (currentHealth > 0)
        {
            Movement();
            SelectTarget();
            Fighting();
        }
        //DeathDisableAction();
        GetBars();
        LevelUp();
    }

    /*void Movement()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
        {
            
            if (Physics.Raycast(ray, out hit, 100))
            {
                attackMode = false;
                isAttacking = false;
                Agent.destination = hit.point;
            }
        }

        if (Agent.remainingDistance <= Agent.stoppingDistance)
        {
            isRunning = false;
        }
        else
        {
            isRunning = true;
        }
        
        Animator.SetBool("isRunning", isRunning);
    }

    void SelectTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Animator.SetBool("isAttacking", isAttacking);
        
        if (Physics.Raycast(ray, out hit, 10000))
        {
            if (hit.transform.tag == "Enemy" && hit.transform.GetComponent<StatsSystem>().currentHealth > 0)
            {
                Status.color = Color.white;
                CurrentEnemyName = hit.transform.GetComponent<StatsSystem>().name;
                CurrentEnemyHealth = hit.transform.GetComponent<StatsSystem>().currentHealth;
                CurrentEnemyMaxHealth = hit.transform.GetComponent<StatsSystem>().maxHealth;
                CurrentEnemyName.ToUpper();
                Status.text.ToUpper();
                
                StatusBar.SetActive(true);
                Status.text = CurrentEnemyName +": Health "+CurrentEnemyHealth+"/"+CurrentEnemyMaxHealth;

                if (Input.GetMouseButtonDown(1))
                {
                    InterfaceSounds.clickToAttack();
                    transform.LookAt(hit.transform);
                    CurrentEnemy = hit.transform;

                    attackMode = true;
                    
                }
               
            }
            else if (hit.transform.tag == "Enemy" && hit.transform.GetComponent<StatsSystem>().currentHealth <= 0)
            {
                Status.color = Color.red;
                
                CurrentEnemyHealth = hit.transform.GetComponent<StatsSystem>().currentHealth;
                CurrentEnemyMaxHealth = hit.transform.GetComponent<StatsSystem>().maxHealth;
                
                StatusBar.SetActive(true);
                Status.text = CurrentEnemyName +": Health 0/"+CurrentEnemyMaxHealth;
            }

            else
            {
                StatusBar.SetActive(false);
                Status.color = Color.white;
            }
        }
        
    }

    void Fighting()
    {
        if (attackMode)
        {
            transform.LookAt(CurrentEnemy.transform);

            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) > 1.5))
            {
                isRunning = true;
                isAttacking = false;
                Agent.destination = CurrentEnemy.position;
            }
            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) < 1.5))
            {
                isRunning = false;
                isAttacking = true;
            }

            if (CurrentEnemy.GetComponent<StatsSystem>().currentHealth <= 0)
            {
                isAttacking = false;
                attackMode = false;
            }
        }
    }

    public void swordDamage()
    {
        characterSounds.PlayOneShot(swordDmg, 1.5F);
    }
    
    public void Death()
    {
        characterSounds.PlayOneShot(deathSnd, 0.8F);
        OnDeathDisableCollider();
        DeathDisableAction();
        isDead = true;
        Animator.SetBool("isDead", isDead);
        
    }

    private void DeathDisableAction()
    {
        if (currentHealth <= 0)
        {
            isAttacking = false;
            isRunning = false;
            attackMode = false;
        }
    }

    public void OnDeathDisableCollider()
    {
        if (currentHealth <= 0)
            characterCollider.enabled = !characterCollider.enabled;
    }

    void GetBars()
    {
        GetComponent<HealthBar>().SetHealth(currentHealth);
        GetComponent<HealthBar>().SetMana(currentMana);
    }
    
    void TakeDamage(int Damage)
    {
        //Debug.Log("Trafienie");
        CurrentEnemy.GetComponent<StatsSystem>().currentHealth -= 5;

        CurrentEnemy.GetComponent<StatsSystem>().MonsterDamagedBySword();
        
        if (CurrentEnemy.GetComponent<StatsSystem>().currentHealth > 0)
            CurrentEnemy.GetComponent<StatsSystem>().MonsterTakeDamage();
        
        if (CurrentEnemy.GetComponent<StatsSystem>().currentHealth <= 0)
            CurrentEnemy.GetComponent<StatsSystem>().Death();
    }
    */
}
