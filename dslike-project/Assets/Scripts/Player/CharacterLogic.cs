﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class CharacterLogic : CharacterSounds
{
    private int CurrentEnemyHealth;
    private int CurrentEnemyMaxHealth;
    [HideInInspector] public bool 
        isDead = false, isRunning = false, isAttacking = false, attackMode = false;
    private Vector3 CurrentEnemyPos;
    private string CurrentEnemyName;
    [HideInInspector]public Animator Animator;
    [HideInInspector]public NavMeshAgent Agent;
    public Transform CurrentEnemy;
    [HideInInspector]public Collider characterCollider;
    [HideInInspector]public AudioSource characterSounds;
    public GameObject StatusBar;
    [HideInInspector]public Text Status;
    public GameObject UICanvas;
    public int currentExp;
    public Camera Playercamera;

    void Start()
    {
        
    }


    void Update()
    {
        
    }
    
    public void Movement()
    {
        SetComponents();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
        {
            
            if (Physics.Raycast(ray, out hit, 100))
            {
                attackMode = false;
                isAttacking = false;
                Agent.destination = hit.point;
            }
        }

        if (Agent.remainingDistance <= Agent.stoppingDistance)
        {
            isRunning = false;
        }
        else
        {
            isRunning = true;
        }
        
        Animator.SetBool("isRunning", isRunning);
    }

    public void SelectTarget()
    {
        SetComponents();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Animator.SetBool("isAttacking", isAttacking);
        
        if (Physics.Raycast(ray, out hit, 10000))
        {
            if (hit.transform.tag == "Enemy" && hit.transform.GetComponent<StatsSystem>().currentHealth > 0)
            {
                Status.color = Color.white;
                CurrentEnemyName = hit.transform.GetComponent<StatsSystem>().name;
                CurrentEnemyHealth = hit.transform.GetComponent<StatsSystem>().currentHealth;
                CurrentEnemyMaxHealth = hit.transform.GetComponent<StatsSystem>().maxHealth;
                CurrentEnemyName.ToUpper();
                Status.text.ToUpper();
                
                StatusBar.SetActive(true);
                Status.text = CurrentEnemyName +": Health "+CurrentEnemyHealth+"/"+CurrentEnemyMaxHealth;

                if (Input.GetMouseButtonDown(1))
                {
                    //InterfaceSounds.clickToAttack();
                    transform.LookAt(hit.transform);
                    CurrentEnemy = hit.transform;

                    attackMode = true;
                    
                }
               
            }
            else if (hit.transform.tag == "Enemy" && hit.transform.GetComponent<StatsSystem>().currentHealth <= 0)
            {
                Status.color = Color.red;
                
                CurrentEnemyHealth = hit.transform.GetComponent<StatsSystem>().currentHealth;
                CurrentEnemyMaxHealth = hit.transform.GetComponent<StatsSystem>().maxHealth;
                
                StatusBar.SetActive(true);
                Status.text = CurrentEnemyName +": Health 0/"+CurrentEnemyMaxHealth;
            }

            else
            {
                StatusBar.SetActive(false);
                Status.color = Color.white;
            }
        }
        
    }

    public void Fighting()
    {
        SetComponents();
        if (attackMode)
        {
            transform.LookAt(CurrentEnemy.transform);

            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) > 1.5))
            {
                isRunning = true;
                isAttacking = false;
                Agent.destination = CurrentEnemy.position;
            }
            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) < 1.5))
            {
                isRunning = false;
                isAttacking = true;
            }

            if (CurrentEnemy.GetComponent<StatsSystem>().currentHealth <= 0)
            {
                isAttacking = false;
                attackMode = false;
            }
        }
    }

    public void Death()
    {
        SetComponents();
        //OnDeathDisableCollider();
        //DeathDisableAction();
        CharacterDeath();
        isDead = true;
        Animator.SetBool("isDead", isDead);
        characterCollider.enabled = !characterCollider.enabled;
        
    }

    /*public void DeathDisableAction()
    {
        SetComponents();
        if (currentHealth <= 0)
        {
            isAttacking = false;
            isRunning = false;
            attackMode = false;
        }
    }*/

    /*public void OnDeathDisableCollider()
    {
        SetComponents();
        if (currentHealth <= 0)
            characterCollider.enabled = !characterCollider.enabled;
    }*/

    

    void SetComponents()
    {
        characterSounds = GetComponent<AudioSource>();
        Animator = GetComponent<Animator>();
        Agent = GetComponent<NavMeshAgent>();
        characterCollider = GetComponent<Collider>();
    }
    
    public void TakeDamage(int Damage)
    {
        //Debug.Log("Trafienie");
        CurrentEnemy.GetComponent<StatsSystem>().currentHealth -= 5;

        CurrentEnemy.GetComponent<StatsSystem>().MonsterDamagedBySword();
        
        if (CurrentEnemy.GetComponent<StatsSystem>().currentHealth > 0)
            CurrentEnemy.GetComponent<StatsSystem>().MonsterTakeDamage();

        if (CurrentEnemy.GetComponent<StatsSystem>().currentHealth <= 0)
        {
            CurrentEnemy.GetComponent<StatsSystem>().Death();
            currentExp += CurrentEnemy.GetComponent<StatsSystem>().expReward;
        }
            
    }
}
