﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatsSystem : CharacterLogic
{
    public int level;
    public int melee;
    public int ranged;
    public int magic;
    
    public int nextLevel;
    
    public int maxHealth;
    public int currentHealth;
    public int maxMana;
    public int currentMana;

    void Start()
    {
        LevelUp();
    }


    void Update()
    {
        OnDeathDisableActions();
        GetBars();

    }

    void OnDeathDisableActions()
    {
        if (currentHealth <= 0)
        {
            isAttacking = false;
            isRunning = false;
            attackMode = false;
        }
    }
    public void GetBars()
    {
        HealthBar healthBar = GetComponent<HealthBar>();
        healthBar.SetMaxHealth(maxHealth);
        healthBar.SetMaxMana(maxMana);
        
        GetComponent<HealthBar>().SetHealth(currentHealth);
        GetComponent<HealthBar>().SetMana(currentMana);
    }

    public void LevelUp()
    {
        if (level == 0)
            nextLevel = 100;
        if (level == 1)
            nextLevel = 500;
        if (level == 2)
            nextLevel = 2500;
        if (level == 3)
            nextLevel = 12500;
        
        if (currentExp >= nextLevel)
        {
            level += 1;
        }
    }
}
