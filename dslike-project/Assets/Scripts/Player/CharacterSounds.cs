﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSounds : MonoBehaviour
{
    public AudioSource Sounds;
    public AudioClip CharacterTakeDamageSnd;
    public AudioClip CharacterTakeDamage2Snd;
    public AudioClip CharacterDamagedBySwordSnd;
    public AudioClip CharacterDamagedByBowSnd;
    public AudioClip CharacterDamagedByClubSnd;
    public AudioClip CharacterDeathSnd;
    
    
    public void CharacterTakeDamage()
    {
        GetComponent<AudioSource>().PlayOneShot(CharacterTakeDamageSnd, 1.0F);
    }
    public void CharacterDamagedBySword()
    {
        GetComponent<AudioSource>().PlayOneShot(CharacterDamagedBySwordSnd, 1.0F);
    }
    public void CharacterDamagedByBow()
    {
        GetComponent<AudioSource>().PlayOneShot(CharacterDamagedByBowSnd, 1.0F);
    }
    public void CharacterDamagedByClub()
    {
        GetComponent<AudioSource>().PlayOneShot(CharacterDamagedByClubSnd, 1.0F);
    }
    
    public void CharacterDeath()
    {
        GetComponent<AudioSource>().PlayOneShot(CharacterDeathSnd, 1.0F);
    }
}
