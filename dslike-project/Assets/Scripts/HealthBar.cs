﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    public Slider healthBar;
    public Slider manaBar;

    public void SetMaxHealth(int maxhealth)
    {
        healthBar.maxValue = maxhealth;
        healthBar.value = maxhealth;
    }

    public void SetHealth(int health)
    {
        healthBar.value = health;
    }

    public void SetMaxMana(int maxmana)
    {
        manaBar.maxValue = maxmana;
        manaBar.value = maxmana;
    }

    public void SetMana(int mana)
    {
        manaBar.value = mana;
    }
}
