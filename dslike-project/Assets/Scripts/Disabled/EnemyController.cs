﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{

    public AudioClip attackSound;
    public AudioClip deathSound;

    public GameObject Player;

    public float detectionRange = 10;

    public float chasingRange = 20;
    public AudioSource enemySounds;
    
    private Animator Animator;
    private NavMeshAgent Agent;
    private Transform CurrentEnemy;
    private Vector3 CurrentEnemyPos;

    private bool isRunning = false;
    private bool isAttacking = false;
    private bool attackMode = false;

    void Start()
    {
        enemySounds = GetComponent<AudioSource>();
        Animator = GetComponent<Animator>();
        Agent = GetComponent<NavMeshAgent>();
    }


    void Update()
    {
        if (!GetComponent<StatsSystem>().isDead)
        {
            DetectPlayer();
            Fighting();
        }
    }

    void DetectPlayer()
    {
        //CurrentEnemy = Player.GetComponent<Transform>();
        //attackMode = true;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, detectionRange);
        if (!attackMode)
        {
            foreach (Collider hitCollider in hitColliders)
            {
                if (hitCollider.tag == "Player")
                {
                    Debug.Log("W zasięgu");
                    CurrentEnemy = hitCollider.GetComponent<Transform>();
                    attackMode = true;
                }
                
            }
        }
    }
    
    void Fighting()
    {
        if (attackMode)
        {
            transform.LookAt(CurrentEnemy.transform);
                    
            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) > 1.5) && (Vector3.Distance(transform.position, CurrentEnemy.transform.position) < chasingRange) )
            {
                isRunning = true;
                isAttacking = false;
                Agent.isStopped = false;
                Agent.destination = CurrentEnemy.position;
            }
            else if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) <= 1.5))
            {
                isRunning = false;
                isAttacking = true;
            }

            if (CurrentEnemy.GetComponent<StatsSystem>().isDead == true)
            {
                isAttacking = false;
                attackMode = false;
            }
            
            if ((Vector3.Distance(transform.position, CurrentEnemy.transform.position) >= chasingRange))
            {
                CurrentEnemy = null;
                isRunning = false;
                isAttacking = false;
                attackMode = false;
                Agent.isStopped = true;
            }
        }
        Animator.SetBool("isRunning", isRunning);
        Animator.SetBool("isAttacking", isAttacking);
    }
    
    
   /* 
    public void SkeletonAttackScream()
    {
        enemySounds.PlayOneShot(attackSound, 1F);
    }

    public void Death()
    {
        enemySounds.PlayOneShot(deathSound, 1F);
    }
    */
}
